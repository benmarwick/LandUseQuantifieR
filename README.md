# LandUseQuantifieR

This package is an (adapted) R implementation of the circle diagram approach from Hughes et al. 2018.

Please check the vignettes for instruction how to use the package.

## Installation

You can install moin from gitlab with:

```r
if(!require('devtools')) install.packages('devtools')
devtools::install_git("https://gitlab.com/CRC1266-A2/LandUseQuantifieR")
```

## Reference

Hughes, R.E., Weiberg, E., Bonnier, A., Finné, M., Kaplan, J.O., 2018. Quantifying Land Use in Past Societies from Cultural Practice and Archaeological Data. Land 7, 9. https://doi.org/10.3390/land7010009