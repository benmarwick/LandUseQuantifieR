##' Calculates the area of a village
##'
##' Calculates the area of the village in hectares based on the amount of inhabitants and a measure of their density
##' @title area_village
##' @param person_per_hectar The amount of people per hectar or population density; default 200, based on Jameson et al. 1994
##' @param population The amount of people living in the village; default 3800, based on Jameson et al. 1994
##' @return A numeric, i.e. the size of a village in hectar
##' @export
area_village <- function(person_per_hectar = 200,
                         population = 3800){
  sqm_per_person <- 10000 / person_per_hectar
  village_size <- population * sqm_per_person / 10000
  return(village_size)
}
