##' Kcal requirements of people for different activity levels and sex
##'
##' @format A data.frame with 74 rows and 7 variables.
##' # the following code is used to create the data set
##' library(rJava)
##' library(tabulizer)
##' report <- "https://www.cnpp.usda.gov/sites/default/files/usda_food_patterns/EstimatedCalorieNeedsPerDayTable.pdf"
##' kcal_req <- extract_tables(report, encoding="UTF-8")
##' kcal_req <- kcal_req[[1]]
##' kcal_req <- kcal_req[6:35,]
##' kcal_req <- gsub(pattern = ",",
##'             replacement = "",
##'             x = kcal_req)
##' kcal_req <- as.data.frame(kcal_req, stringsAsFactors = FALSE)

##' names(kcal_req) <- c("age",
##'                 "male_sedentary","male_moderately_active","male_active",
##'                 "female_sedentary","female_moderately_active","female_active")
##' kcal_req[,2:7] <- sapply(kcal_req[,2:7], as.numeric)

##' kcal_req_age <- kcal_req[18:29,] %>%
##'   dplyr::mutate(from = lapply(strsplit(kcal_req[18:29,1], split = "–"), function(x) x[1]),
##'                 to = lapply(strsplit(kcal_req[18:29,1], split = "–"), function(x) x[2])) %>%
##'   dplyr::rowwise() %>%
##'   dplyr::do(data.frame(age = as.character(seq(.$from, .$to, by = 1)),
##'                        male_sedentary = .$male_sedentary,
##'                        male_moderately_active = .$male_moderately_active,
##'                        male_active = .$male_active,
##'                        female_sedentary = .$female_sedentary,
##'                        female_moderately_active = .$female_moderately_active,
##'                        female_active = .$female_active)) %>%
##'   dplyr::bind_rows(kcal_req[1:17,], .)

##' usethis::use_data(kcal_req_age, overwrite = TRUE)
##'
##' @source \url{https://origin.www.cnpp.usda.gov/Publications/USDAFoodPatterns/EstimatedCalorieNeedsPerDayTable.pdf}
"kcal_req_age"

##' Kcal requirements of people for different age, activity levels and sex
##'
##' @format A data.frame with 444 rows and 4 varibles:
##'
##' \describe{
##'   \item{age}{The age of the person}
##'   \item{sex}{The sex of the person}
##'   \item{activity}{The activity level of the person}
##'   \item{kcal_req}{The daily kcal requirements for this type of person}
##' }
##'
##'kcal_req_detailed <- read.table("./kcal_req_age_long.csv",
##'                      header = TRUE,
##'                      colClasses = c(age = "character"),
##'                      sep = ",",
##'                      stringsAsFactors = FALSE) %>%
##' dplyr::select(-X1)
##'
##' usethis::use_data(kcal_req_detailed, overwrite = TRUE)
##' @source \url{https://origin.www.cnpp.usda.gov/Publications/USDAFoodPatterns/EstimatedCalorieNeedsPerDayTable.pdf}
"kcal_req_detailed"

##' Contains informations of the daily protein requirements
##'
##' @format A data.frame with 75 rows and 3 variables:
##'
##' \describe{
##'   \item{age}{The age of the person}
##'   \item{male_g_day}{The daily requirement of protein in g of a male persons}
##'   \item{female_g_day}{The daily requirement of protein in g of a female persons}
##' }
##'
##'protein_req_age <- read.table("./protein_req_age.csv",
##'                      header = TRUE,
##'                      sep = ";",
##'                      stringsAsFactors = FALSE)
##' usethis::use_data(protein_req_age, overwrite = TRUE)
##' @source \url{https://www.dge.de/wissenschaft/referenzwerte/protein/}
"protein_req_age"

##' Contains informations of the daily protein requirements
##'
##' @format A data.frame with 75 rows and 3 variables:
##'
##' \describe{
##'   \item{age}{The age of the person}
##'   \item{sex}{The sex of the person}
##'   \item{protein_req_g}{The daily requirement of protein in g of this type of person}
##' }
##'
##'protein_req_age_long <- read.table("./protein_req_age_long.csv",
##'                      header = TRUE,
##'                      colClasses = c(age = "character"),
##'                      sep = ";",
##'                      stringsAsFactors = FALSE)
##' usethis::use_data(protein_req_age_long, overwrite = TRUE)
##' @source \url{https://www.dge.de/wissenschaft/referenzwerte/protein/}
"protein_req_age_long"


##' Contains informations of the plant data
##'
##' @format A data.frame with 46 rows and 16 variables:
##'
##' \describe{
##'   \item{Type}{The type of plant}
##'   \item{Species}{The specific plant species}
##'   \item{productivity}{The productivity of the plant in kg/ha}
##'   \item{produc_units}{The units of the productivity}
##'   \item{calories}{The caloric content of the plant in kcal/kg}
##'   \item{calories_units}{The units of the calories}
##'   \item{protein}{The protein content of the plant in g/kg}
##'   \item{planting}{Percentage of the yield needed for planting in the next year}
##'   \item{waste_loss}{Percentage of the yield that is lost}
##'   \item{storage}{Percentage of the yield that is stored}
##'   \item{protein_units}{The units of the protein}
##'   \item{yield_units}{The units of the yield}
##'   \item{reference_calories}{The references of calories data}
##'   \item{reference_productivity}{The references of productivity data}
##'   \item{reference_protein}{The references of protein data}
##'   \item{reference_planting_loss_storage}{The references of the planting, loss and storage data}
##' }
##'
##'plants <- read.table("./plants.csv",
##'                      header = TRUE,
##'                      sep = ";",
##'                      stringsAsFactors = FALSE)
##' usethis::use_data(plants, overwrite = TRUE)
##'
"plants"

##' Contains informations of the wood consumption
##'
##' @format A data.frame with 1 row and 2 variables:
##'
##' \describe{
##'   \item{consumption_energy_person_a_GJ}{The amount of energy one person consumes per year by burning wood in GJ}
##'   \item{consumption_fuelwood_person_a_kg}{The amount of fuelwood a person uses in kg}
##'   }
##'
##' wood_cons <- read.table("./wood_cons.csv",
##'                     header = TRUE,
##'                     sep = ";",
##'                     stringsAsFactors = FALSE)
##' usethis::use_data(wood_cons, overwrite = TRUE)
##'
"wood_cons"

##' Contains informations of the wood productivity
##'
##' @format A data.frame with 2 row and 4 variables:
##'
##' \describe{
##'   \item{forest_type}{The Type of wood that is used by the people/villagers}
##'   \item{productivity_in_cbm_per_ha_per_a}{The productivity of the forest/woodland in cbm per ha and year}
##'   \item{calorific_value_in_MJ_per_kg}{The calorific value of the wood in MJ per kg}
##'   \item{references}{The references of the data}
##'   }
##'
##' wood_prod <- read.table("./wood_prod.csv",
##'                     header = TRUE,
##'                     sep = ";",
##'                     stringsAsFactors = FALSE)
##' usethis::use_data(wood_prod, overwrite = TRUE)
##'
"wood_prod"

##' Contains informations of the wood requirements for bronze production
##'
##' @format A data.frame with 1 row and 4 variables:
##'
##' \describe{
##'   \item{energy_req_melt_copper}{The amount of energy required to melt copper in KJ; default 60000 KJ}
##'   \item{energy_req_melt_tin}{The amount of energy required to melt tin in KJ; default 59000 KJ}
##'   \item{energy_req_melt_metals}{The amount of energy required to melt metal in KJ; default 59000 KJ}
##'   \item{weight_melted_bronze_person}{The amount of weighted metal per person (kg/capita); default 2}
##'   }
##'
##' bronze_production <- read.table("./bronze_production.csv",
##'                     header = TRUE,
##'                     sep = ";",
##'                     stringsAsFactors = FALSE)
##' usethis::use_data(bronze_production, overwrite = TRUE)
##'
"bronze_production"

##' Contains informations of the flax production
##'
##' @format A data.frame with 1 row and 4 variables:
##'
##' \describe{
##'   \item{Productivity}{The productivity of the flax in kg/ha;  default 804}
##'   \item{Linen_need_kg}{The amount of linen needed (kg); default .1}
##'   \item{Flax_kg_in_100g_linen}{The amount of flax (kg) per 100g linen; default 1}
##'   \item{PropPop}{The proportion of the population that uses flax; default .65}
##'   }
##'
##' flax_production <- read.table("./flax_production.csv",
##'                     header = TRUE,
##'                     sep = ",",
##'                     stringsAsFactors = FALSE)
##' usethis::use_data(flax_production, overwrite = TRUE)
##'
"flax_production"

##' Contains informations of the ceramics production
##'
##' @format A data.frame with 1 row and 5 variables:
##'
##' \describe{
##'   \item{vessels_person}{The number of vessels per person}
##'   \item{mean_weight_vessel_kg}{The mean weight of a vessel in kg}
##'   \item{mean_weight_wine_amphora_kg}{The mean weight of a wine amphora in kg}
##'   \item{mean_weight_oil_amphora_kg}{The mean weight of an oil amphora in kg}
##'   \item{ratio_wood_ceramics}{The ratio of wood to ceramics}
##'   }
##'
##' ceramics_production <- read.table("./ceramics_production.csv",
##'                     header = TRUE,
##'                     sep = ";",
##'                     stringsAsFactors = FALSE)
##' usethis::use_data(ceramics_production, overwrite = TRUE)
##'
"ceramics_production"

##' Contains informations of iron production
##'
##' @format A data.frame with 1 row and 3 variables:
##'
##' \describe{
##'   \item{iron_person_kg}{The amount of iron needed per person in kg; default 2; source:  Weiberg et al. (in prep.)}
##'   \item{ratio_charcoal_iron}{The ratio of charcoal to iron; default 82}
##'   \item{ratio_charcoal_wood}{The ratio of charcoal to wood; default 4}
##'   }
##'
##' iron_production <- read.table("./iron_production.csv",
##'                     header = TRUE,
##'                     sep = ";",
##'                     stringsAsFactors = FALSE)
##' usethis::use_data(iron_production, overwrite = TRUE)
##'
"iron_production"

##' Contains informations of the animal data
##'
##' @format A data.frame with 14 rows and 10 variables:
##'
##' \describe{
##'   \item{animal}{The type of animal}
##'   \item{type}{The subtype of the animal}
##'   \item{calory_requirement}{The caloric requirements in kcal/kg}
##'   \item{meat_yield_factor}{The factor of the calculation of the animals weight that remains as meat}
##'   \item{meat_yield}{The meat yield in kg; calculated as weight * meat_yield_factor}
##'   \item{reference}{The references for the animal data}
##'   \item{herd_composition}{The composition of the herd of each animal}
##'   \item{reference_composition}{The references of the herd composition}
##'   \item{slaughter_factor}{The slaughter factor}
##'   \item{reference_sf}{The reference for the slaughter factor}
##' }
##'
##' animals <- read.table("./animals.csv",
##'                      header = TRUE,
##'                      sep = ";",
##'                      stringsAsFactors = FALSE)
##' usethis::use_data(animals, overwrite = TRUE)
##'
"animals"

##' Contains informations of the animal products data
##'
##' @format A data.frame with 15 rows and 6 variables:
##'
##' \describe{
##'   \item{animal}{The type of animal}
##'   \item{product}{The specific animal product}
##'   \item{calories}{The caloric content of the animal product in kcal/kg}
##'   \item{protein}{The protein content of the animal product in g/kg}
##'   \item{reference_calories}{The references of the caloric content}
##'   \item{reference_protein}{The reference of the protein content}
##' }
##'
##' animal_products <- read.table("./animal_products.csv",
##'                      header = TRUE,
##'                      sep = ";",
##'                      stringsAsFactors = FALSE)
##' usethis::use_data(animal_products, overwrite = TRUE)
##'
"animal_products"


##' Contains informations of the game data
##'
##' @format A data.frame with 11 observations and 8 variables:
##'
##' \describe{
##'   \item{animal}{The general category of the game animal: deer, wild boar, birds, fish}
##'   \item{type}{The type of the animal}
##'   \item{weight}{The weight of one individual in kg}
##'   \item{meat_yield_factor}{The factor of the calculation of the animals weight that remains as meat}
##'   \item{meat_yield}{The meat yield in kg; calculated as weight * meat_yield_factor}
##'   \item{calories}{The caloric content in kcal/kg}
##'   \item{protein}{The protein content in g/kg}
##'   \item{references_weight}{The references of the weight data}
##'   \item{references_calories}{The references of the caloric data}
##'   \item{references_protein}{The references of the protein data}
##' }
##'
##' game <- read.table("./game.csv",
##'                    header = TRUE,
##'                    sep = ";",
##'                    stringsAsFactors = FALSE)
##' usethis::use_data(game, overwrite = TRUE)
"game"

##' Contains informations of the calory requirements for humans
##'
##' @format A data.frame with 80 rows and 6 variables:
##'
##' \describe{
##'   \item{pal}{The Total Energy Expenditure for 24 hours expressed as a multiple of Basal Metabolic Rate. Calculated as TEE/BMR for 24 hours (cf.FAO2001).}
##'   \item{age_category}{The age categorized as a human younger than 19 years or older than 19 years}
##'   \item{sex}{The gender}
##'   \item{age}{The age expressed as category}
##'   \item{kcal_day}{The caloric requirement as kcal/day}
##'   \item{reference}{The sources of literature for the required kcal data}
##'   }
##'
##' kcal_req_human <- read.table("./calory_requirement_human.csv",
##'                     header = TRUE,
##'                     sep = ";",
##'                     stringsAsFactors = FALSE)
##' usethis::use_data(kcal_req_human, overwrite = TRUE)
##'
"kcal_req_human"

##' Contains informations of the dairy products data
##'
##' @format A data.frame with 13 rows and 9 variables:
##'
##' \describe{
##'   \item{product}{The type of milk}
##'   \item{milk_per_indiv_and_year}{The amount of milk available for human consumption per individual and year}
##'   \item{unit_milk}{The unit of the milk; L}
##'   \item{calories}{The amount of calories}
##'   \item{unit_calories}{The unit of the calories; kcal/L}
##'   \item{reference}{The references}
##'   \item{protein}{The amount of protein in g/L}
##'   \item{reference_protein}{The references}
##'   \item{calory_req_day_individual}{The amount of calories needed to feed one individual per day; kcal}
##' }
##'
##' milk <- read.table("./milk.csv",
##'                      header = TRUE,
##'                      sep = ";",
##'                      stringsAsFactors = FALSE)
##' usethis::use_data(milk, overwrite = TRUE)
##'
"milk"

##' Contains informations of the cheese data
##'
##' @format A data.frame with 13 rows and 8 variables:
##'
##' \describe{
##'   \item{product}{The type of cheese}
##'   \item{calories_per_kg}{The amount of calories per kg cheese}
##'   \item{reference_calories}{The references}
##'   \item{protein}{The amount of protein per kg cheese}
##'   \item{reference_protein}{The references}
##'   \item{milk_per_indiv_and_year}{The amount of milk for human consumption per individual and year}
##'   \item{liter_milk_needed_for_one_kg_cheese}{The amount of milk needed to produce 1 kg of cheese}
##'   \item{calory_req_day_individual}{The amount of calories needed to feed one individual per day; kcal}
##' }
##'
##' cheese <- read.table("./cheese.csv",
##'                      header = TRUE,
##'                      sep = ";",
##'                      stringsAsFactors = FALSE)
##' usethis::use_data(cheese, overwrite = TRUE)
##'
"cheese"


##' Contains informations of the auxiliary animals
##'
##' @format A data.frame with 3 rows and 1 variable:
##'
##' \describe{
##'   \item{type}{The type of auxiliary animal}
##'   \item{Area_ploughed_day_ha}{The Area that can be ploughed per day and team of animals in ha}
##'   \item{calory_req_day_individual}{The amount of calories needed to feed one individual per day; kcal}
##' }
##'
##' aux_animals <- read.table("./aux_animals.csv",
##'                      header = TRUE,
##'                      sep = ";",
##'                      stringsAsFactors = FALSE)
##' usethis::use_data(aux_animals, overwrite = TRUE)
##'
"aux_animals"
